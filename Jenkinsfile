pipeline {
    agent any
    environment {
        AUTHOR = 'Fabrice ESTRABAUT'
        PURPOSE    = 'This is a sample Django app'
        LIFE_QUOTE = 'The greatest glory in living lies not in never falling, but in rising every time we fall.'
        /* required by Django application */
        REGISTRY = "registry.gitlab.com/fabrice.estrabaut/django-dockerising"
    }
    stages {
        stage('Checkout') {
            steps {
                // Checkout your source code repository
                git branch: 'master',
                    url: 'https://gitlab.com/fabrice.estrabaut/django-dockerising.git'
            }
        }
        stage('Setup') {
            steps {
                // Set up your virtual environment or any other dependencies
                sh 'python3 -m venv venv'
            }
        }
        stage('Install Dependencies') {
            steps {
                // Install required dependencies
                sh '. venv/bin/activate && pip3 install -r requirements.txt'
            }
        }
        stage('Test') {
            steps {
                // Run your Django tests
                sh '. venv/bin/activate && python3 manage.py test'
            }
        }
        stage('SonarQube Analysis') {
            environment {
                scannerHome = tool 'sonar-scanner'
            }
            steps {
                withSonarQubeEnv('sq') {
                    sh "${scannerHome}/bin/sonar-scanner"
                }
            }
        }
        stage('Build') {
            environment {
                /* fecth credentials for Gitlab */
                DOCKER_CREDS = credentials('gitlab_docker')
                COMMIT_SHA = """${sh(
                    returnStdout: true,
                    script: "git log -n 1 --pretty=format:'%H'"
                )}"""
            }
            steps {
                // Build your Django application
                echo "user=${DOCKER_CREDS_USR} password=${DOCKER_CREDS_PSW}"
                /* Deploy your Django application */
                sh "docker build -t ${REGISTRY}:${COMMIT_SHA} ."
                sh "docker login -u ${DOCKER_CREDS_USR} -p ${DOCKER_CREDS_PSW} registry.gitlab.com"
                sh "docker push ${REGISTRY}:${COMMIT_SHA}" 
            }
        }
    }
    post
    {
        success
        {
            build job: 'Django-simple-deploy', wait: true, propagate: false
        }
    }
}